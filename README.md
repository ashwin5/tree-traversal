# Tree Traversal

Tree- Traversal benchmarking using graphs
Your program will generate two files
A list of nodes with properties that looks as follows. Create the nodes with only the properties and then write a program to calculate the aggproperty

| NodeID | Property |    AggProperty |
|--------|----------|----------------|
| 1   |    5     |     calculated
| 2   |   6      |     calculatated
| 3   |    8     |     calculated


A list of relationships that shows the parent child relationship

|Node ID  |   ParentNodeID|
|---------|---------------|
|1        |   NULL    |
|2         |  1       |
|3         |  1       |


A procedure to cacluate the agg property that will take the properties of children sum them up and then add it to nodeid property and then store it. So our data should look as follows:
|NodeID | Property  |  AggProperty |
|-------|-----------|--------------|
|1      | 5         | 5 + (6 + 8) = 19 |
|2      | 6         | 6 + 0 (no children)|
|3      | 8         | 8 + 0 (no children)|


You will write a program to calculate the aggregate property without using the Graph and using the graph and then plot the time taken for the total calculation using the following metrics

|#ofLevels(Tree)     |  AvgLeafs per node  |  Total Number of Nodes in the tree   |     Time taken|
|--------------------|---------------------|--------------------------------------|---------------|
|1                   |    2                |           3                          |     1sec      |




You will implement the algorithm and test for binary trees with levels between 50 to 300 and provide the output.

You will then write a reflection paper on the same


